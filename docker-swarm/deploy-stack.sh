#!/usr/bin/env bash

DEPLOYMENT_NAME=java-app

# To create a manager node
docker swarm init

# To deploy both services from a stack
docker stack deploy -c ./docker-compose.yml $DEPLOYMENT_NAME

# list all of your deployments
docker stack ls

# List all of your services
docker service ls


# Inspect service logs
# docker service logs $SERVICE_NAME

# Remove your deployemnt
# docker stack rm $DEPLOYMENT_NAME

# Check your service through URLs

#$  http://localhost:8080/actuator/health
#{"status":"UP"}

# Check this URL for a UI dockersamples visualizer
# http://localhost:9090


# Turn off you node
# docker swarm leave --force