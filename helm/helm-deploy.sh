#!/usr/bin/env bash

#-- variables --
NAMESPACE=saif
CHART_PATH=my-app/values.yaml
TILLER_NAMESPACE=kryptonite
RELEAS_NAME=my-app
#-- secret variables --
PSCI_REGISTRY_URL=docker.io
PSCI_REGISTRY_USERNAME=saifalnuaimi
PSCI_REGISTRY_PASSWORD=P@ssw0rdd

#---------------------------------------------
kubectl create secret docker-registry docker-hub-secret \
    -n ${NAMESPACE} \
    --docker-server=${PSCI_REGISTRY_URL} \
    --docker-username=${PSCI_REGISTRY_USERNAME} \
    --docker-password=${PSCI_REGISTRY_PASSWORD} \
    -o yaml --dry-run | kubectl replace -n $NAMESPACE --force -f -
#---------------------------------------------
helm upgrade \
--install -f ${CHART_PATH} \
${RELEAS_NAME} \
my-app \
--force \
--tiller-namespace ${TILLER_NAMESPACE} \
--namespace ${NAMESPACE}
#-------------------------------------------

#helm delete --purge ${RELEAS_NAME} --tiller-namespace=${TILLER_NAMESPACE}