#!/usr/bin/env bash

helm install --name nginx-ingress stable/nginx-ingress
kubectl apply -f ingress-resource.yaml
