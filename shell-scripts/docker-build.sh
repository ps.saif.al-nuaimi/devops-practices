#!/usr/bin/env bash

#--------variables--------
DOCKER_IMAGE=devops-plan
DOCKER_TAG=$(date '+%Y%m%d')-$( git rev-parse --short HEAD)
DOCKER_USER=developer
DOCKER_PASSWORD=FBvAgfFHV8k8eHUh
#-------------------------

cd ..

#login to register
docker login harbor.progressoft.io -u $DOCKER_USER -p $DOCKER_PASSWORD

#docker build and tag
docker build -t  "${DOCKER_USER}":"${DOCKER_IMAGE}":"${DOCKER_TAG}" -f cicd/docker/Dockerfile .

#docker push
docker push "${DOCKER_USER}":"${DOCKER_IMAGE}":"${DOCKER_TAG}"

#docker remove builded image
docker rmi "${DOCKER_USER}":"${DOCKER_IMAGE}":"${DOCKER_TAG}"
