#!/usr/bin/env bash

# install gitlab-runner
# apt install gitlab-runner
#https://www.tutorialspoint.com/gitlab/gitlab_installation.htm

#vairables
INSTANCE_URL=https://gitlab.com/
# your runner token
TOKEN=""

# why do you need this runner
DESCRIPTION=""

# name of the tag runner
RUNNER_TAG_NAME=""

# what the execurtor of this runner for ex  > ssh, docker+machine, docker-ssh+machine, kubernetes, docker, parallels, virtualbox, docker-ssh, shell
EXECUTOR=""

# if docker what image do you need for ex >  alpine:latest
DOCKER_IMAGE=""

#install local gitlab runner

gitlab-runner register \
  --non-interactive \
  --url $INSTANCE_URL \
  --registration-token $TOKEN \
  --executor $EXECUTOR \
  --docker-image $DOCKER_IMAGE \
  --description $DESCRIPTION \
  --tag-list $RUNNER_TAG_NAME \
  --run-untagged="true" \
  --locked="false" \
  --access-level="not_protected"