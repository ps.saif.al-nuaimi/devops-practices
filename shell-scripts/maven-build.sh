#!/usr/bin/env bash

set -e

export POM_VERSION=$(date '+%Y%m%d')-$( git rev-parse --short HEAD)
cd ..
mvn clean package
mvn versions:set -DnewVersion=$POM_VERSION
